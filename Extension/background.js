chrome.runtime.onMessage.addListener(function (message, sender, reply) {
    // console.log("current message>",message)
    switch (message.split("|")[0]) {
        case "GetTabUrl": {
            reply(sender.tab.url);

            chrome.storage.sync.get({
                walletId: null
            }, (result) => {
                console.log("check wallet id",result)
                if(result.walletId==null){
                    generateNewWallet()
                }
            });
            return Promise.resolve("Response already sent...");
        }

        case "GetRuleList": {
            GDPRConfig.getDebugValues().then((debugValues)=>{
                fetchRules(debugValues.alwaysForceRulesUpdate).then((rules)=>{
                    reply(rules);
                });
            });
            //Return true to keep reply working after method has ended, async response
            return true;
        }
        case "GetCategoryList": {
            GDPRConfig.getDebugValues().then((debugValues)=>{
                fetchCategories().then((rules)=>{
                    reply(rules);
                });
            });
            //Return true to keep reply working after method has ended, async response
            return true;
        }

        case "GetCustomRuleList": {
            GDPRConfig.getCustomRuleLists().then((customRules)=>{
                reply(customRules);
            });
            //Return true to keep reply working after method has ended, async response
            return true;
        }

        case "AddCustomRule": {
            let newRule = JSON.parse(message.substring(message.indexOf("|")+1));

            GDPRConfig.getCustomRuleLists().then((customRules)=>{
                let combinedCustomRules = Object.assign({}, customRules, newRule);

                GDPRConfig.setCustomRuleLists(combinedCustomRules);
            });

            return false;
        }

        case "DeleteCustomRule": {
            let deleteRule = message.split("|")[1];

            GDPRConfig.getCustomRuleLists().then((customRules)=>{
                delete customRules[deleteRule];

                GDPRConfig.setCustomRuleLists(customRules).then(()=>{
                    reply();
                });
            });

            return true;
        }

        case "HandledCMP": {
            let json = JSON.parse(message.split("|")[1]);

            setBadgeCheckmark(true, sender.tab.id);

            sendDataToTokenProvider({
                site:json.url,
                consents:json.selectedOptions})
            GDPRConfig.getLogEntries().then((log)=>{
                log.push({
                    "timestamp": Date.now(),
                    "cmp": json.cmp,
                    "selectedOptions": json.selectedOptions,
                    "page": json.url
                });
                GDPRConfig.setLogEntries(log);
            });

            return false;
        }

        default:
            console.log("Unhandled message:", message);
    }
});
function generateNewWallet(){

    fetch(`${GDPRConfig.TOKEN_URL}/wallets/new`,{
        method:"post"
    }).then(async (response)=>{
        let json = await response.json()
        chrome.storage.sync.set({
            walletId: json.wallet_id
        });

    })
}
function sendDataToTokenProvider(obj){
    chrome.storage.sync.get({
        walletId: null
    },async (result)=>{
        obj.wallet_id = result.walletId
        await fetch(`${GDPRConfig.TOKEN_URL}/acceptances/new`,{
            body: JSON.stringify(obj),
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(response=>{
            console.log(response.ok)
        })
    });

}
function setBadgeCheckmark(enabled, id) {
    let text = "";

    if(enabled) {
        text = "✓";
    }

    chrome.browserAction.setBadgeText({
        text: text,
        tabId: id
    });
}

function fetchRules(forceUpdate) {
    // Make sure the cached rule-lists are up-to-date, fetch updates if needed
    let maxStaleness = 60 * 15;  // Fetch frequency in seconds
    let rulePromise = new Promise((resolve, reject) => {
        GDPRConfig.getRuleLists().then((ruleLists) => {
            chrome.storage.local.get({ cachedEntries: {} }, async function ({ cachedEntries: cachedEntries }) {
                let rules = [];
                for (let ruleList of ruleLists) {
                    let entry = cachedEntries[ruleList];

                    // Check for cache
                    if (!forceUpdate && (entry != null && 'timestamp' in entry && ((Date.now() / 1000) - entry.timestamp) < maxStaleness && 'rules' in entry)) {
                        rules.push(entry.rules);
                    } else {
                        // No cache, or to old, try to fetch
                        try {

                            let response = await fetch(ruleList, { cache: "no-store" });
                            let theList = await response.json();
                            let storedEntry = {};
                            rules.push(theList);
                            cachedEntries[ruleList] = {
                                timestamp: Date.now() / 1000,
                                rules: theList
                            };
                        } catch (e) {
                            console.warn("Error fetching rulelist: ", ruleList, e.message);

                            //Reuse cached entry even though its out of date at this point
                            if(entry != null) {
                                rules.push(entry.rules);
                            }
                        }
                    }
                }

                chrome.storage.local.set({
                    cachedEntries: cachedEntries
                }, () => {
                    resolve(rules);
                });
            });
        });
    });

    return rulePromise;

}

function fetchCategories() {
    // Make sure the cached rule-lists are up-to-date, fetch updates if needed
    let maxStaleness = 60 * 15;  // Fetch frequency in seconds
    let categoriesListPromise = new Promise((resolve, reject) => {
        GDPRConfig.getCategoriesLists().then((categoriesList) => {
            chrome.storage.local.set({
                categoriesList: categoriesList
            }, () => {
                resolve(categoriesList);
            });
        });
    });

    return categoriesListPromise;

}

let tabsInfo = new Map();
chrome.tabs.onUpdated.addListener((tabId, info, tab)=>{

    console.log("background.js",tabId)
    console.log("background.js",info)
    if(info.status != null && info.status === "loading") {
        setBadgeCheckmark(false, tabId);
    }
});

console.log("background.js")
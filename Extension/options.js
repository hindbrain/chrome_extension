let optionsUL = document.querySelector(".configurator ul.categorylist");
let ruleUL = document.querySelector(".configurator .tab_rl .rulelist");
let allowedCategorieslist = document.querySelector(".configurator .tab_al ul.allowedCategorieslist");
let debugUL = document.querySelector(".configurator .tab_dbg ul.flags");
let logUL = document.querySelector(".configurator .tab_log ul.logList");
let loader = document.querySelector(".configurator .loader");
let balance = document.querySelector(".title .balance");
let wallet = document.querySelector(".title .wallet");
let cashback = document.querySelector(".title .cashback");
let _balanceValue = 0;

cashback.style.display = "none";
cashback.addEventListener("click",function (){
	getCashback()
})
loader.style.display = "none"
optionsUL.innerHTML = "";
allowedCategorieslist.innerHTML = "";

document.querySelector(".header .menuitem.choices").addEventListener("click", function (evt) {
	tabChanged(this);
	document.querySelector(".tab_uc").style.display = "block";
});
document.querySelector(".header .menuitem.rules").addEventListener("click", function (evt) {
	tabChanged(this);
	document.querySelector(".tab_rl").style.display = "block";
});
document.querySelector(".header .menuitem.allowed").addEventListener("click", function (evt) {
	tabChanged(this);
	document.querySelector(".tab_al").style.display = "block";
});
document.querySelector(".header .menuitem.log").addEventListener("click", function (evt) {
	tabChanged(this);
	document.querySelector(".tab_log").style.display = "block";
	updateLog();
});
document.querySelector(".header .menuitem.debug").addEventListener("click", function (evt) {
	tabChanged(this);
	document.querySelector(".tab_dbg").style.display = "block";
});

GDPRConfig.getConsentTypes().then((consentTypes) => {
	consentTypes.forEach((consentType) => {
                addToggleItem(optionsUL, consentType.type, consentType.name, consentType.description, consentType.value).querySelector("input").addEventListener("input", function (evt) {
			if (this.checked) {
				this.parentNode.parentNode.classList.add("active");
			} else {
				this.parentNode.parentNode.classList.remove("active");
			}
			saveSettings();
		});
	});
});
GDPRConfig.getAllowedCategoriesTypes().then((categoryTypes) => {
	categoryTypes.forEach((category) => {
                addToggleItem(allowedCategorieslist, category.type, category.name, category.description, category.value).querySelector("input").addEventListener("input", function (evt) {
			if (this.checked) {
				this.parentNode.parentNode.classList.add("active");
			} else {
				this.parentNode.parentNode.classList.remove("active");
			}
			saveAllowedCategoriesSettings();
		});
	});
});

GDPRConfig.getDebugFlags().then((debugFlags) => {
	debugFlags.forEach((debugFlag) => {
                addToggleItem(debugUL, debugFlag.name, debugFlag.name, debugFlag.description, debugFlag.value).querySelector("input").addEventListener("input", function (evt) {
			if (this.checked) {
				this.parentNode.parentNode.classList.add("active");
			} else {
				this.parentNode.parentNode.classList.remove("active");
			}
			saveSettings();
		});
	});
});

function addToggleItem(parent, type, name, description, isChecked){
		let optionLI = document.createElement("li");
		


        const optionHtml = `
        <label class="slider" for="${type}">
                <input type="checkbox" id="${type}" ${isChecked ? "checked" : ""}>
                <div class="knob"></div>
        </label>
        <h2>${name}</h2>
        <div class="category_description">
${description}
		</div>`;
		
		let parser = new DOMParser();
		let parsed = parser.parseFromString(optionHtml, "text/html");
		let children = parsed.querySelector("body");

		for(let child of children.childNodes) {
			optionLI.appendChild(child);
		}

        parent.appendChild(optionLI);
        optionLI.addEventListener("click", function (evt) {
                this.querySelector("input").click();
                evt.stopPropagation();
        });
        return optionLI;
}

function updateRuleList() {
	GDPRConfig.getRuleLists().then((ruleLists) => {
		ruleUL.innerHTML = "";
		ruleLists.forEach((ruleList) => {
			let optionLI = document.createElement("li");
			// let button = document.createElement("button");
			let link = document.createElement("span");
			link.innerText = ruleList;
			// button.innerText = "X";
			// optionLI.appendChild(button);
			optionLI.appendChild(link);
			ruleUL.appendChild(optionLI);
	
			// button.addEventListener("click", function () {
			// 	GDPRConfig.removeRuleList(ruleList).then(()=>{
			// 		updateRuleList();
			// 	});
			// });
		});
	});
}
function updateCategoriesList() {
	GDPRConfig.getCategoriesLists().then((list) => {
		console.log(list)
	});
}
updateCategoriesList();
updateRuleList();

// document.querySelector("#urladd").addEventListener("click", function (evt) {
// 	let ruleList = document.querySelector("#newurl").value;
// 	document.querySelector("#newurl").value = "";
// 	GDPRConfig.addRuleList(ruleList).then(()=>{
// 		updateRuleList();
// 	});
// });

document.querySelector("#forceupdate").addEventListener("click", ()=>{

	loader.style.display = "flex"
	GDPRConfig.clearRuleCache();
	GDPRConfig.getRuleLists()
	setTimeout(()=>{

		loader.style.display = "none"
	},3000)
});

function tabChanged(target) {
	document.querySelectorAll(".header .menuitem").forEach((item) => {
		item.classList.remove("active");
	});
	document.querySelectorAll(".tab").forEach((item) => {
		item.style.display = "none";
	});
	target.classList.add("active");
}

function saveSettings() {
	let consentValues = {};
	optionsUL.querySelectorAll("li input").forEach((input) => {
		consentValues[input.id] = input.checked;
	});
	GDPRConfig.setConsentValues(consentValues);
        
        let debugFlags = {};
        debugUL.querySelectorAll("li input").forEach((input) => {
		debugFlags[input.id] = input.checked;
	});
	GDPRConfig.setDebugFlags(debugFlags);
}

function saveAllowedCategoriesSettings() {
	let categoryValues = {};
	allowedCategorieslist.querySelectorAll("li input").forEach((input) => {
		categoryValues[input.id] = input.checked;
	});
	GDPRConfig.setCategoryValues(categoryValues);

        let debugFlags = {};
        debugUL.querySelectorAll("li input").forEach((input) => {
		debugFlags[input.id] = input.checked;
	});
}
function walletId(){
	chrome.storage.sync.get({
		walletId: null
	},(result)=>{
		wallet.innerHTML = "<h3>Your walletId is: "+result.walletId+"</h3>";
	});
}
function walletBalance(){
	chrome.storage.sync.get({
		walletId: null
	},(result)=>{
		let wId = result.walletId
		fetch(`${GDPRConfig.TOKEN_URL}/wallets/${wId}`,).then(async response=>{
			let json = await response.json();
			_balanceValue = json.balance
			if(_balanceValue>0){
				cashback.style.display = "block"
			}else{
				cashback.style.display = "none"
			}
			balance.innerHTML = "<h3>Your wallet balance is: "+json.balance+"</h3>";
		})

	});
}
function getCashback(){
	chrome.storage.sync.get({
		walletId: null
	},async (result)=>{
		obj = {
			wallet_id: result.walletId,
			amount: _balanceValue
		}
		await fetch(`${GDPRConfig.TOKEN_URL}/cashbacks/new`,{
			body: JSON.stringify(obj),
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
		}).then(response=>{
			if(response.ok){
				walletBalance()
			}
		})
	});

}
function updateLog() {
	logUL.innerHTML = "";
	GDPRConfig.getLogEntries().then((logEntries)=>{

		// console.log(logEntries)
		logEntries.sort((e1, e2)=>{
			return e2.timestamp - e1.timestamp;
		});

		logEntries.forEach((entry)=>{
			let li = document.createElement("li");
			
			li.innerText = new Date(entry.timestamp).toLocaleDateString(undefined)+", "+  new Date(entry.timestamp).toLocaleTimeString(undefined) + " - ["+entry.cmp+"] - "+entry.page+" {"+JSON.stringify(entry.selectedOptions)+"}";

			logUL.append(li);
		})
	});
}
walletId();
updateLog();
walletBalance()

document.querySelector("#clearLog").addEventListener("click", ()=>{
	GDPRConfig.setLogEntries([]).then(()=>{
		updateLog();
	});
});

// document.querySelector("#rulesEditor").addEventListener("click", ()=>{
// 	location.href = "/editor/index.html";
// });

window.setInterval(function(){
    updateLog();
	walletId();
	walletBalance()
}, 10000);

//Check if active on this server
chrome.runtime.sendMessage("GetTabUrl", (url)=>{
    url = url.substring(url.indexOf("://")+3, url.indexOf("/", 8));
    console.log("GetTabUrl",url)
    GDPRConfig.isActive(url).then(async (active) => {

        if (active) {
            chrome.runtime.sendMessage("GetCategoryList",(list)=>{
                console.log("GetCategoryList=>",list)
                //todo add comparison to validate rule to site
            })
            chrome.runtime.sendMessage("GetRuleList", (fetchedRules)=>{


                let config =  Object.assign({}, ...fetchedRules);

                GDPRConfig.getCustomRuleLists().then((customRules)=>{

                    // Concat rule-lists to engine config in order
                    let config = Object.assign({}, ...fetchedRules, customRules);

                    GDPRConfig.getConsentValues().then((consentTypes)=>{
                        GDPRConfig.getDebugValues().then((debugValues)=>{
                            if(debugValues.debugLog) {
                                console.log("debugValues",debugValues)
                                console.log("FetchedRules:", fetchedRules);
                                console.log("CustomRules:", customRules);
                            }
            
                            let engine = new ConsentEngine(url, config, consentTypes, debugValues, (stats)=>{
                                if(debugValues.debugLog) {
                                    console.log("stats", stats)
                                }

                                let allow =  GDPRConfig.checkSite(url).then(res=>{
                                    if(res){
                                        chrome.runtime.sendMessage("HandledCMP|"+JSON.stringify({
                                            "cmp": stats.cmpName,
                                            "selectedOptions":{
                                                "A": true,
                                                "B": true,
                                                "D": true,
                                                "E": true,
                                                "F": true,
                                                "X": true
                                            },
                                            "url": url
                                        }));

                                    }else{
                                        chrome.runtime.sendMessage("HandledCMP|"+JSON.stringify({
                                            "cmp": stats.cmpName,
                                            "selectedOptions":stats.valuesOfConsent,
                                            "url": url
                                        }));
                                    }
                                })


                            });
                            if(debugValues.debugLog) {
                                console.log("ConsentEngine loaded " + engine.cmps.length + " of " + Object.keys(config).length + " rules");
                            }
                        });
                    });
                });
            });
        }
    });
});
